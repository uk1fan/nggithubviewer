(function() {
    
    var app = angular.module("githubViewer");

    var MainController = function($scope, $log, $location) {

        $scope.search = function(userName) {
            $log.info("Searching for " + userName);
            $location.path("/user/" + userName);
        };

        $scope.userName = "kwechsler";
    };

    app.controller("MainController", ["$scope", "$log", "$location", MainController]);

}() );