(function() {
    
    var app = angular.module("githubViewer", ["ngRoute"]);
    
    app.config(function($routeProvider) {
        $routeProvider
            .when("/main", {
                templateUrl: "main.html",
                controller: "MainController"
            })
            .when("/user/:userName", {
                templateUrl: "user.html",
                controller: "UserController"
            })
            .when("/repo/:userName/:repoName", {
                templateUrl: "repo.html",
                controller: "RepoController"
            })
            .otherwise({redirectTo: "/main"});
    });
    
}());