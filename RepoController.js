(function() {
    
    var app = angular.module("githubViewer");

    var RepoController = function($scope, github, $routeParams) {
        
        var repoName = $routeParams.repoName;
        var userName = $routeParams.userName;
        
        var onRepo = function(data) {
            $scope.repo = data;
        };
        
        var onError = function(reason) {
            $scope.error = "Could not retrieve repo from Github.  " + reason;
            $scope.user = null;
        };

        github.getRepoDetails(userName, repoName)
            .then(onRepo, onError);
    };
    
    app.controller("RepoController", ["$scope", "github", "$routeParams", RepoController]);

}() );