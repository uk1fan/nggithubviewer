(function() {
    
    var github = function($http) {
      
        // User - https://api.github.com/users/:userName
        var getUser = function(userName) {
            return $http.get("https://api.github.com/users/" + userName)
                        .then(function(response) {
                            return response.data;
                        });
        };
        
        // Repos - https://api.github.com/users/:userName/repos
        var getRepos = function(user) {
            return $http.get(user.repos_url)
                        .then(function(response) {
                            return response.data;  
                        });
        };
        
        // Repo Detail - https://api.github.com/repos/:userName/:repo
        var getRepoDetails = function(userName, repoName) {
            var repo;
            var repoURL = "https://api.github.com/repos/" + userName + "/" + repoName;
            
            return $http.get(repoURL)
                        .then(function(response) {
                            repo = response.data;
                            return $http.get(repoURL + "/contributors");  
                        })
                        .then(function(response) {
                            repo.contributors = response.data;
                            return repo;
                        });
        };

        return {
            getUser: getUser,
            getRepos: getRepos,
            getRepoDetails: getRepoDetails
        }
    };
    
    var module = angular.module("githubViewer");
    module.factory("github", github);
    
}());