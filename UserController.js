(function() {
    
    var app = angular.module("githubViewer");

    var UserController = function($scope, github, $routeParams) {

        var onUserComplete = function(data) {
            $scope.user = data;
            github.getRepos($scope.user).then(onRepos, onError);
        };
        
        var onRepos = function(data) {
            $scope.repos = data;
        };
        
        var onError = function(reason) {
            $scope.error = "Could not retrieve data from Github."
            $scope.user = null;
        };

        $scope.message = "Github Viewer";
        $scope.userName = $routeParams.userName;
        $scope.repoSortOrder = "-stargazers_count";
        github.getUser($scope.userName).then(onUserComplete, onError);
    };

    app.controller("UserController", ["$scope", "github", "$routeParams", UserController]);

}() );